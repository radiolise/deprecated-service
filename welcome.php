<?php
  $document = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $swr3 = "?url=http://swr-swr3-live.cast.addradio.de/swr/swr3/live/mp3/128/stream.mp3";
?>
<!DOCTYPE html>
<html>
  <title>Radiolise Web Service</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="favicon.ico">
  <link rel="stylesheet" href="welcome.css">
  <body>
    <div id="content">
      <h1>Radiolise Web Service</h1>
      <hr>
      <h2>Developer API</h2>
      <div class="page">
        <h3>What is Radiolise?</h3>
        <p>Radiolise is a free/libre web application that allows you to play your favorite TV and radio streams. You can easily access the user interface and receive and play streams via your favorite web browser.</p>
        <p><a href="https://radiolise.com/" style="font-weight: bold">Try Radiolise</a></p>
        <h3>About the API</h3>
        <p>The actual purpose of the web service is to provide additional information (such as now playing data) about a specific radio station to keep users of Radiolise up to date on current song names or headlines.</p>
        <p>To make it easier for you to benefit from the API as well and to improve it, I have decided to offer it as free/libre software, too.</p>
        <h3>How it works</h3>
        <p>To use the API, just provide a GET or POST request parameter named ‘url’ which contains the URL to the livestream itself.</p>
        <p>You will get a JSON object containing the following keys if they are available:</p>
        <ul>
          <li>MIME type (‘content-type’)</li>
          <li>Station description (‘description’)</li>
          <li>Genre (‘genre’)</li>
          <li>Station name (‘name’)</li>
          <li>Now playing information (‘title’)</li>
          <li>Website (‘url’)</li>
        </ul>
        <p class="left"><b>Example w/ SWR3</b> (GET method): <a href="<?=$swr3?>"><?=$document . $swr3?></a></p>
        <h3>Licensing</h3>
        <div class="mono">
          <p>&copy; 2018-2020 Marco Bauer</p>
          <p>Licensed under the Apache License, Version 2.0 (the "License");<br>you may not use this file except in compliance with the License.<br>You may obtain a copy of the License at</p>
          <p> &nbsp; &nbsp; <a href="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</a></p>
          <p>Unless required by applicable law or agreed to in writing, software<br>distributed under the License is distributed on an "AS IS" BASIS,<br>WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.<br>See the License for the specific language governing permissions and<br>limitations under the License.</p>
        </div>
      </div>
    </div>
  </body>
</html>
